﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lasmart.Web;
using Lasmart.Web.Controllers;
using Lasmart.Data.Business.UnitOfWorks.Interfaces;
using Lasmart.Data.Business.UnitOfWorks.Services;

namespace Lasmart.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void CupboardsCount()
        {
            IUnitOfWork unitOfWork = new UnitOfWork();
            var cupboards = unitOfWork.Cupboards.GetAll();
            Assert.IsTrue(cupboards.Count() == 3);
        }

        
    }
}
